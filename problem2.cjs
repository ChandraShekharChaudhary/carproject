let inventory =require('./inventory.cjs');


function lastCarInInventory(inventory){

    if(!inventory){
        return [];
    }
    if(typeof inventory === 'undefined' || inventory.length===0 || !Array.isArray(inventory)){
        return []; 
    }
    
    if(inventory.length>0 ){
        let lc=inventory.length-1;
        if(inventory[lc]['car_make'] != undefined && inventory[lc]['car_model'] != undefined){

            return `Last car is a ${inventory[lc]['car_make']} ${inventory[lc]['car_model']}`;
        }
    }
    return [];
    
}


module.exports = lastCarInInventory;