function carYearList(inventory){
    if(!inventory){
        return [];
    }
    if(inventory.length>0 ){
        let lc=inventory.length-1;
        if(inventory[lc]['car_make'] != undefined && inventory[lc]['car_model'] != undefined){

            let yearList=[];
            for(let index=0; index<inventory.length; index++){
                yearList.push(inventory[index]['car_year']);
            }
            return yearList;
        }
    }
    return [];
    
}

module.exports = carYearList;