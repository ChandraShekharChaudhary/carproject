let inventory = require('../inventory.cjs');
let lastCarInInventory=require('../problem2.cjs');

// Some test cases for the problem2 solution
console.log(lastCarInInventory(inventory));
console.log(lastCarInInventory());
console.log(lastCarInInventory([]));
console.log(lastCarInInventory({abc:10}));
console.log(lastCarInInventory(10));
console.log(lastCarInInventory('String'));
console.log(lastCarInInventory(8.95));
console.log(lastCarInInventory({id: 33, name: "Test", length: 10}));
console.log(findlastCarInInventoryCarById([{"id":0,"car_make":"Dodge","car_model":"Magnum","car_year":2008}]))