let inventory = require("../inventory.cjs");
let findCarById= require("../problem1.cjs");

//Some test cases for the problem1 solution
console.log(findCarById(inventory,22));
console.log(findCarById());     //what if we dose not pass any argument in the function?
console.log(findCarById(inventory)); // what if we dosent pass any id for the car?
console.log(findCarById([], 33));  //what if we pass an empty array in place of inventory?
console.log(findCarById({}, 22));    //What if we pass an empty object in place of inventory?
console.log(findCarById(inventory, 'string'));
console.log(findCarById(inventory , 55));
console.log(findCarById(55));
console.log(findCarById(inventory, -5));
console.log(findCarById("string" , 5));
console.log(findCarById(inventory, 33));
console.log(findCarById({id: 33, name: "Test", length: 10}, 33));
console.log(findCarById([{"id":0,"car_make":"Dodge","car_model":"Magnum","car_year":2008}], 0))