function BMWandAUDI(inventory){
    if(!inventory){
        return [];
    }
    let BMWandAUDI_List=[];
    for(let index=0; index< inventory.length; index++){
        if(inventory[index]['car_make']=='Audi' || inventory[index]['car_make']=='BMW'){
            BMWandAUDI_List.push(inventory[index]);
        }
    }
    if(BMWandAUDI_List.length==0){
        return [];
    }
    return BMWandAUDI_List;
}

module.exports = BMWandAUDI;