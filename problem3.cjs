let inventory =require('./inventory.cjs');

function sortModelName(inventory){
    if(!inventory){
        return [];
    }
    if(inventory.length>0 ){
        let lc=inventory.length-1;
        if(inventory[lc]['car_make'] != undefined && inventory[lc]['car_model'] != undefined){

            inventory.sort((a, b) => {
                let fa = a.car_model.toLowerCase(),
                 fb = b.car_model.toLowerCase();
            
                if (fa < fb) {
                    return -1;
                }
                if (fa > fb) {
                    return 1;
                }
                return 0;
            });
        
            let modelList=[];
            for(let index=0; index<inventory.length-1; index++){
                modelList.push(inventory[index]['car_model']);
            }
          
            return modelList;

        }
    }
    return [];
    
}

module.exports = sortModelName;