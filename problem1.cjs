// let inventory =require('./inventory.cjs');


function findCarById(inventory, carId) {
    if (!Array.isArray(inventory) || inventory.length === 0 || typeof carId !== 'number') {
      return [];
    }
    
    for (let index = 0; index < inventory.length; index++) {
      if (inventory[index].id === carId) {
        return inventory[index];
      }
    }
  
    // Car not found
    return [];
  }
  
  module.exports = findCarById;
  