let carYearList=require('./problem4.cjs');

function oldCarList(inventory){
    if(!inventory){
        return [];
    }

    let yearList=carYearList(inventory);
    if(yearList==undefined){
        return [];
    }
    let oldCarYear=[];
    // console.log(yearList);
    for(let index=0; index<yearList.length; index++){
        if(yearList[index]<2000){
            oldCarYear.push(yearList[index]);
        }
    }
    return oldCarYear.length;

}

module.exports = oldCarList;